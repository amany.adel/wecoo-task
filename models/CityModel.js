class City {
    constructor() {
        this.db = require('../db/db.js');
    }

    async addCity(name,governmentId) {
        try {
           var result= await this.db.execute('INSERT INTO city (gov_id,name) VALUES ( ?,?)', [governmentId,name]

            )
                .then(result => {
                    // this.db.end();
                    let res='city is added';
                    return res;
                }).catch(err => {
                    console.log(err);
                    return err;
                });
        }
        catch (e) {
            return e;
        }

        return result;

    }

    async getAllCity() {
        try {
           var result= await this.db.execute('select * from city')
                .then(result => {
                    // this.db.end();
                    let res=result[0];
                    return res;
                }).catch(err => {
                    console.log(err);
                    return err;
                });
        }
        catch (e) {
            return e;
        }

        return result;

    }

    async deleteCity(cityId) {
        try {
           var result= await this.db.execute('DELETE FROM city WHERE id=?',[cityId])
                .then(result => {
                    // this.db.end();
                    let res='city is deleted';
                    return res;
                }).catch(err => {
                    console.log(err);
                    return err;
                });
        }
        catch (e) {
            return e;
        }

        return result;

    }





}

module.exports = City;