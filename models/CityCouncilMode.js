class Council {
    constructor() {
        this.db = require('../db/db.js');
    }

    async addCouncil(name,cityId) {
        try {
           var result= await this.db.execute('INSERT INTO city_council (city_id,name) VALUES ( ?,?)', [cityId,name]

            )
                .then(result => {
                    // this.db.end();
                    let res='council is added';
                    return res;
                }).catch(err => {
                    console.log(err);
                    return err;
                });
        }
        catch (e) {
            return e;
        }

        return result;

    }

    async getAllCouncil() {
        try {
           var result= await this.db.execute('select * from city_council')
                .then(result => {
                    // this.db.end();
                    let res=result[0];
                    return res;
                }).catch(err => {
                    console.log(err);
                    return err;
                });
        }
        catch (e) {
            return e;
        }

        return result;

    }

    async deleteCouncil(councilId) {
        try {
           var result= await this.db.execute('DELETE FROM city_council WHERE id=?',[councilId])
                .then(result => {
                    // this.db.end();
                    let res='council is deleted';
                    return res;
                }).catch(err => {
                    console.log(err);
                    return err;
                });
        }
        catch (e) {
            return e;
        }

        return result;

    }





}

module.exports = Council;