class Area {
    constructor() {
        this.db = require('../db/db.js');
    }

    async addArea(name,cityCouncilId) {
        try {
           var result= await this.db.execute('INSERT INTO area (city_council_id,name) VALUES ( ?,?)', [cityCouncilId,name]

            )
                .then(result => {
                    // this.db.end();
                    let res='area is added';
                    return res;
                }).catch(err => {
                    console.log(err);
                    return err;
                });
        }
        catch (e) {
            return e;
        }

        return result;

    }

    async getAllArea() {
        try {
           var result= await this.db.execute('select * from area')
                .then(result => {
                    // this.db.end();
                    let res=result[0];
                    return res;
                }).catch(err => {
                    console.log(err);
                    return err;
                });
        }
        catch (e) {
            return e;
        }

        return result;

    }

    async deleteArea(areaId) {
        try {
           var result= await this.db.execute('DELETE FROM area WHERE id=?',[areaId])
                .then(result => {
                    // this.db.end();
                    let res='area is deleted';
                    return res;
                }).catch(err => {
                    console.log(err);
                    return err;
                });
        }
        catch (e) {
            return e;
        }

        return result;

    }





}

module.exports = Area;