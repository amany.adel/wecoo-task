class Government {
    constructor() {
        this.db = require('../db/db.js');
    }

    async addGovernment(name) {
        try {
           var result= await this.db.execute('INSERT INTO government (name) VALUES ( ?)', [name]

            )
                .then(result => {
                    // this.db.end();
                    let res='government is added';
                    return res;
                }).catch(err => {
                    console.log(err);
                    return err;
                });
        }
        catch (e) {
            return e;
        }

        return result;

    }

    async getAllGovernment() {
        try {
           var result= await this.db.execute('select * from government')
                .then(result => {
                    // this.db.end();
                    let res=result[0];
                    return res;
                }).catch(err => {
                    console.log(err);
                    return err;
                });
        }
        catch (e) {
            return e;
        }

        return result;

    }

    async deleteGovernment(cityId) {
        try {
           var result= await this.db.execute('DELETE FROM government WHERE id=?',[cityId])
                .then(result => {
                    // this.db.end();
                    let res='government is deleted';
                    return res;
                }).catch(err => {
                    console.log(err);
                    return err;
                });
        }
        catch (e) {
            return e;
        }

        return result;

    }





}

module.exports = Government;