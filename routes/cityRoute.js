const express = require('express');
const router = new express.Router();

const cityConstoller=require('../controllers/cityController.js');

//================== insert new City ===========================//

router.post('/insert',cityConstoller.insertCity);

//================== get all City ===========================//

router.get('/all_city',cityConstoller.allCityRender);

//================== delete City by id ===========================//

router.post('/delete_city',cityConstoller.deleteCity);



module.exports = router