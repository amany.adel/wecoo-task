const express = require('express');
const router = new express.Router();

const governmentConstoller=require('../controllers/governmentController.js');

//================== insert new Government ===========================//

router.post('/insert',governmentConstoller.insertGovernment);

//================== get all Government ===========================//

router.post('/all_government',governmentConstoller.allGovernment);

//================== get all Government ===========================//

router.get('/all_government',governmentConstoller.allGovernmentRender);

//================== delete Government by id ===========================//

router.post('/delete_government',governmentConstoller.deleteGovernment);



module.exports = router