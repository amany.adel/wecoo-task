const express = require('express');
const router = new express.Router();

const areaController=require('../controllers/areaController.js');

//================== insert new City ===========================//

router.post('/insert',areaController.insertArea);

//================== get all City ===========================//

router.get('/all_area',areaController.allAreaRender);

//================== delete City by id ===========================//

router.post('/delete_area',areaController.deleteArea);



module.exports = router