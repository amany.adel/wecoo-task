const express = require('express');
const router = new express.Router();

const cityCouncilController=require('../controllers/cityCouncilController.js');

//================== insert new City ===========================//

router.post('/insert',cityCouncilController.insertCouncil);

//================== get all City ===========================//

router.get('/all_council',cityCouncilController.allCouncilRender);

//================== delete City by id ===========================//

router.post('/delete_council',cityCouncilController.deleteCouncil);



module.exports = router