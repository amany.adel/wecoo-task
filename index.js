const express = require("express");
const app = new express();

const server = require("http").createServer(app);
const socket = require("socket.io");
const io = socket.listen(server);

var path = require('path');
const cors = require("cors");
const bodyParser = require("body-parser");
const validator = require('validator');


const db = require('./db/db.js');
const Government = require('./models/GovernmentModels.js');
const governmentObject = new Government();

const governmentRoute = require('./routes/governmentRoute.js');
const cityRoute = require('./routes/cityRoute.js');
const cityCouncilRoute = require('./routes/cityCouncilRoute.js');
const areaRoute = require('./routes/areaRoute.js');

const governmentController = require('./controllers/governmentController.js');
const cityController = require('./controllers/cityController.js');
const cityCouncilController = require('./controllers/cityCouncilController.js');
const areaController = require('./controllers/areaController.js');


// using ejs engine
app.set('view engine', 'ejs')
app.set('views', 'views')

app.use(bodyParser.json({ type: "application/*+json" }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.json());
app.use(bodyParser.raw({ type: "application/vnd.custom-type" }));
app.use(bodyParser.text({ type: "text/html" }));
app.use(cors());


app.use('/government', governmentRoute);
app.use('/city', cityRoute);
app.use('/council', cityCouncilRoute);
app.use('/area', areaRoute);



server.listen(8800, function () {
  console.log("Express server listening on port 8800");


});


io.on("connection", function (client) {
  console.log("client connected ");

  client.on("allGovernment", async () => {
    government = await governmentController.allGovernmentSocket();
    io.emit("allGovernment", government);
  });

  client.on("allCity", async () => {
    city = await cityController.allCitySocket();
    io.emit("allCity", city);
  });

  client.on("allCouncil", async () => {
    council = await cityCouncilController.allCouncilSocket();
    io.emit("allCouncil", council);
  });

  client.on("allArea", async () => {
    area = await areaController.allAreaSocket();
    io.emit("allArea", area);
  });

});