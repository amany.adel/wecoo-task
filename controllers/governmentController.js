const Government=require('../models/GovernmentModels.js');
const governmentObject=new Government();

const validator = require('validator');


exports.insertGovernment=async (req,res)=>
{
    let governmentName=req.body.governmentName;
    if(validator.isEmpty(governmentName))
    {
        res.send('plz,enter government name');
    }
    var result=await governmentObject.addGovernment(governmentName);
    res.send(result);
}


exports.allGovernment=async (req,res)=>
{
    
    var result=await governmentObject.getAllGovernment();
    res.send(result);
}

exports.allGovernmentRender=async (req,res)=>
{
    
    res.render('index')
}

exports.deleteGovernment=async (req,res)=>
{
    let governmentId=req.body.governmentId;
    if(validator.isEmpty(governmentId))
    {
        res.send('plz,enter government id');
    }
    var result=await governmentObject.deleteGovernment(governmentId);
    res.send(result);
}

exports.allGovernmentSocket=async (req,res)=>
{
    
    var result=await governmentObject.getAllGovernment();
    return result;
}