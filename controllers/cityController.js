const City=require('../models/CityModel.js');
const cityObject=new City();

const validator = require('validator');


exports.insertCity=async (req,res)=>
{
    let cityName=req.body.cityName;
    let governmentId=req.body.governmentId;
    if(validator.isEmpty(cityName) || validator.isEmpty(governmentId) )
    {
        res.send('plz,enterempty faild');
    }
    var result=await cityObject.addCity(cityName,governmentId);
    res.send(result);
}


exports.allCity=async (req,res)=>
{
    
    var result=await cityObject.getAllCity();
    res.send(result);
}
exports.allCityRender=async (req,res)=>
{
    
    res.render('city');
}

exports.deleteCity=async (req,res)=>
{
    let cityId=req.body.cityId;
    if(validator.isEmpty(cityId)  )
    {
        res.send('plz,enterempty faild');
    }
    var result=await cityObject.deleteCity(cityId);
    res.send(result);
}


exports.allCitySocket=async (req,res)=>
{
    
    var result=await cityObject.getAllCity();
    return result;
}