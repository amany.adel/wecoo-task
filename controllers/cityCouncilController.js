const Council=require('../models/CityCouncilMode.js');
const councilObject=new Council();

const validator = require('validator');


exports.insertCouncil=async (req,res)=>
{
    let councilName=req.body.councilName;
    let cityId=req.body.cityId;
    if(validator.isEmpty(councilName) || validator.isEmpty(cityId) )
    {
        res.send('plz,enterempty faild');
    }
    var result=await councilObject.addCouncil(councilName,cityId);
    res.send(result);
}


exports.allCouncil=async (req,res)=>
{
    
    var result=await councilObject.getAllCouncil();
    res.send(result);
}
exports.allCouncilRender=async (req,res)=>
{
    
    res.render('council');
}

exports.deleteCouncil=async (req,res)=>
{
    let councilId=req.body.councilId;
    if(validator.isEmpty(councilId)  )
    {
        res.send('plz,enterempty faild');
    }
    var result=await councilObject.deleteCouncil(councilId);
    res.send(result);
}

exports.allCouncilSocket=async (req,res)=>
{
    
    var result=await councilObject.getAllCouncil();
    return result;
}