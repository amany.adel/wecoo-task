const Area=require('../models/AreaModel.js');
const areaObject=new Area();

const validator = require('validator');


exports.insertArea=async (req,res)=>
{
    let areaName=req.body.areaName;
    let councilId=req.body.councilId;
    if(validator.isEmpty(areaName) || validator.isEmpty(councilId) )
    {
        res.send('plz,enterempty faild');
    }
    var result=await areaObject.addArea(areaName,councilId);
    res.send(result);
}


exports.allArea=async (req,res)=>
{
    
    var result=await areaObject.getAllArea();
    res.send(result);
}
exports.allAreaRender=async (req,res)=>
{
    
    res.render('area');
}

exports.deleteArea=async (req,res)=>
{
    let areaId=req.body.areaId;
    if(validator.isEmpty(areaId)  )
    {
        res.send('plz,enterempty faild');
    }
    var result=await areaObject.deleteArea(areaId);
    res.send(result);
}

exports.allAreaSocket=async (req,res)=>
{
    
    var result=await areaObject.getAllArea();
    return result;
}